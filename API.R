library(wsmichael)
library(plumber)
library(jsonlite)
library(httr)

#* @get /test
#* @serializer json
function(){
  mysummary(c(0:10,50))
}

#* @get /plot
#* @serializer png
function(){
  rand <- rnorm(100)
  hist(rand)
}

#* @get /cases/<country>
#* @serializer png list(width = 400, height = 500)
function(country){
  fr <- wsmichael::casesCountry(country)
  plot(X8~as.POSIXct(X10),data=fr,type='l')

}

